import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.css']
})
export class StarRatingComponent implements OnInit {

  @Input('rating')  rating = 3;
  @Input('starCount')  starCount = 5;
  @Input('color') color='accent';
  // @Output() private ratingUpdated = new EventEmitter();

  private snackBarDuration= 2000;
  ratingArr: number[] = [];
  constructor() { }

  ngOnInit(): void {
    for (let index:number = 0; index < this.starCount; index++) {
      this.ratingArr.push(index);
    }
  }

  showIcon(index:number) {
    if (this.rating >= index + 1) {
      return 'star';
    } else {
      return 'star_border';
    }
  }

}

export enum StarRatingColor {
  primary = "primary",
  accent = "accent",
  warn = "warn"
}
