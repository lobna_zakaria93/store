import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainAppComponent } from './main-app/main-app.component';
import { ProductAppComponent } from './products/product-app/product-app.component';
import { ProductListComponent } from './products/product-list/product-list.component';



const routes: Routes = [
 {path:'product-list' , loadChildren:() => import('./products/products.module').then(m => m.ProductsModule), component:ProductListComponent},
 {path:'add-product' , loadChildren:() => import('./products/products.module').then(m => m.ProductsModule), component:ProductAppComponent},
 {path:'product' , loadChildren:() => import('./products/products.module').then(m => m.ProductsModule)},
 {path:'main',component:MainAppComponent},
 {path:'', redirectTo:'main',pathMatch:'full'}
];

@NgModule({
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(routes, { useHash: false}),

  ],
 })

export class AppRoutingModule { }