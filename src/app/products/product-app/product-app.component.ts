import { Component,  OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-product-app',
  templateUrl: './product-app.component.html',
  styleUrls: ['./product-app.component.css']
})
export class ProductAppComponent implements OnInit {

  productForm:FormGroup;
  categories = [];
  constructor(private fb: FormBuilder, private router:Router, private productService:ProductService,private snackBar: MatSnackBar,) { }

  ngOnInit(): void {
    this.initProductForm();
    this.getCategories()
  }

  initProductForm(){
    this.productForm = this.fb.group({
      title:[''],
      category:[''],
      price:[''],
      description:[''],
      image:['']
    })
  }

  getCategories(){
    this.productService.getCategories().subscribe((res:any) => {
      this.categories = res;
    })
  }
  onSubmitClicked(){
    let body = this.productForm.value;
    this.productService.addProduct(body).subscribe((res:any) => {
      this.snackBar.open('Product '+res.title+ ' Has Been Added Successfully With New Id '+ res.id, 'action', {
        duration: 4000,
      });
      // this.router.navigateByUrl('/')
    })
    
  }

}
