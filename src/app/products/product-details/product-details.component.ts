import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StarRatingColor } from 'src/app/_shared/star-rating/star-rating.component';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  id:any;

  productDetails:any;
  starColor:StarRatingColor = StarRatingColor.accent;
  starColorP:StarRatingColor = StarRatingColor.primary;
  starColorW:StarRatingColor = StarRatingColor.warn;

  starCount:number = 5;
  constructor(private route:ActivatedRoute, private productService:ProductService) { 
    this.id =this.route.snapshot.params.id
  }

  ngOnInit(): void {
    this.getProductDetail();
  }

  getProductDetail(){
    this.productService.getProductDetails(this.id).subscribe(res => {
      this.productDetails=res;
    })
  }

}
