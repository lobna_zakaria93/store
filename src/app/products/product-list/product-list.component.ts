import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ProductService } from '../services/product.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})


export class ProductListComponent implements OnInit{
  product_data =[];

  displayedColumns: string[] = ['title', 'price', 'category','action'];
  dataSource:any;


  constructor(private router:Router, private productService: ProductService) { }

  ngOnInit(): void {
    this.getProductList()
    
  }

  getProductList(){
    this.productService.getProductList().subscribe((res:any) => {
      console.log(res)
      this.product_data = res;
      this.dataSource = new MatTableDataSource(this.product_data);
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onViewProduct(id:any){
    this.router.navigateByUrl(`/product/${id}`)
  }

}
