import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductAppComponent } from './product-app/product-app.component';
import { ProductDetailsComponent } from './product-details/product-details.component';
import { ProductListComponent } from './product-list/product-list.component';



const routes: Routes = [
 {path:'' , component:ProductListComponent },
 {path:'add-product', component:ProductAppComponent},
 {path:':id', component:ProductDetailsComponent}
];

@NgModule({
  exports: [RouterModule],
  imports: [
    RouterModule.forChild(routes),

  ],
 })

export class ProductsRoutingModule { }