import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  listUrl = 'https://fakestoreapi.com/products?limit=10';
  detailUrl = 'https://fakestoreapi.com/products/';
  addUrl = 'https://fakestoreapi.com/products';
  categories = 'https://fakestoreapi.com/products/categories';

  constructor(private http:HttpClient) { }

  getProductList(){
    return this.http.get(this.listUrl)
  }

  getProductDetails(id:any){
    return this.http.get(this.detailUrl+id);
  }

  getCategories(){
    return this.http.get(this.categories)
  }

  addProduct(body:any){
    return this.http.post(this.addUrl,body)
  }
}
